package com.atlassian.bitbucket.coverage.rest;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommitCoverageEntity {

    @JsonProperty("files")
    private List<FileCoverageEntity> files;

    public List<FileCoverageEntity> getFiles() {
        return files;
    }

    public void setFiles(List<FileCoverageEntity> files) {
        this.files = files;
    }

}
