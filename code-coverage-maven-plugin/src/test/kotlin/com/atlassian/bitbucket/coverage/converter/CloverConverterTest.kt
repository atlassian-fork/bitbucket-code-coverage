package com.atlassian.bitbucket.coverage.converter

import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.hasSize
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import java.io.File
import java.nio.file.Paths

class CloverConverterTest {

    private val cloverConverter = CloverConverter()

    @Test
    fun readCloverXmlReport() {
        val reportFile = File(this::class.java.classLoader.getResource("clover-report.xml").file)
        val report = cloverConverter.convert(reportFile, Paths.get("/Users/wkritzinger/dev/bb/bb"))

        assertThat(report.files, hasSize(605))
        assertThat(report.files[0].path, equalTo("service-impl/src/main/java/com/atlassian/stash/internal/mode/ApplicationModeGuard.java"))
        assertThat(report.files[0].coverage, equalTo("C:30,32,33,36,38,40,41,42,43,46,47,48,53;P:;U:"))
        assertThat(report.files[1].path, equalTo("service-impl/src/main/java/com/atlassian/stash/internal/migration/entity/repository/RepositoryAttachmentsExporter.java"))
        assertThat(report.files[11].coverage, equalTo("C:29,30,31,38,40,48,50,56,58,64,66,77,79,82,84,88,90,93,95,98,100,101;P:86;U:"))
        assertThat(report.files[14].coverage, equalTo("C:61,68,69,70,71,72,73,75,89,92,95,97,99,100,101,102,104,110,111,112,115,116,117,118,120,124,131,134,136,139,141,143,144,147,148,150,156,157,158,159,160,161,162,164,165,169,170,174,180,181,184,189,203,204,206,211,214,215,218,222,230,231,232,233,237,238,239,246,247,250,251,254,255,257,260,261,262,263,264,267,268,269,272,273,274,277,278,283,284,285,286,288,289,291,293,294,296,297,300,301,304,307,310,315,319,320,321,322,323,325,326,327,330,331,334;P:96,140,182,207,216,241;U:79,106,152,208,209,242,336"))
        assertThat(report.files[18].path, equalTo("service-impl/src/main/java/com/atlassian/stash/internal/cluster/LicenseClusterJoinCheck.java"))
        assertThat(report.files[18].coverage, equalTo("C:;P:;U:26,28,31,34,37,40,43,46,49,51,54,57,60,61"))
        assertThat(report.files[604].path, equalTo("service-impl/src/main/java/com/atlassian/stash/internal/attach/DefaultAttachmentService.java"))
    }
}
