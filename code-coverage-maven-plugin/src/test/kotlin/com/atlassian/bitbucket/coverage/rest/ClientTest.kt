package com.atlassian.bitbucket.coverage.rest

import org.junit.Test
import java.net.URL

class ClientTest {

    private val bitbucketUrl: URL = URL("http://abc.d")

    @Test(expected = IllegalArgumentException::class)
    fun emptyPasswordThrowsExceptionWithNonEmptyUser() {
        Client(bitbucketUrl, "user", "", "", null)
    }

    @Test(expected = IllegalArgumentException::class)
    fun emptyUserAndTokenThrowsException() {
        Client(bitbucketUrl, "", "", "", null)
    }

}
