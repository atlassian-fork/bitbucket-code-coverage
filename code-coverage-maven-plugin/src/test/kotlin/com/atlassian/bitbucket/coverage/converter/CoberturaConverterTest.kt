package com.atlassian.bitbucket.coverage.converter

import com.atlassian.bitbucket.coverage.cobertura.Line
import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.COVERED_KEY
import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.PARTLY_COVERED_KEY
import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.UNCOVERED_KEY
import org.hamcrest.Matchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import java.io.File
import java.nio.file.Paths

class CoberturaConverterTest {

    private val coberturaConverter = CoberturaConverter()

    @Test
    fun readCoberturaXmlReport() {
        val reportFile = File(this::class.java.classLoader.getResource("cobertura-report.xml").file)
        coberturaConverter.convert(reportFile, Paths.get("."))
    }

    @Test
    fun convertPartlyCoveredBranch() {
        val result = coberturaConverter.convertConditionCoverage("50% (1/2)")
        assertThat(result, `is`(PARTLY_COVERED_KEY))
    }

    @Test
    fun convertCoveredBranch() {
        val result = coberturaConverter.convertConditionCoverage("100% (2/2)")
        assertThat(result, `is`(COVERED_KEY))
    }

    @Test
    fun convertLineWithoutHitsReturnsUncovered() {
        val line: Line = Mockito.mock(Line::class.java)
        `when`(line.hits).thenReturn("0")
        val result = coberturaConverter.convertLine(line)
        assertThat(result, `is`(UNCOVERED_KEY))
    }

    @Test
    fun convertLineWithHitsReturnsCovered() {
        val line: Line = Mockito.mock(Line::class.java)
        `when`(line.hits).thenReturn("1")
        val result = coberturaConverter.convertLine(line)
        assertThat(result, `is`(COVERED_KEY))
    }

}
