package com.atlassian.bitbucket.coverage.converter

import com.atlassian.bitbucket.coverage.XmlReader
import com.atlassian.bitbucket.coverage.cobertura.Class
import com.atlassian.bitbucket.coverage.cobertura.Coverage
import com.atlassian.bitbucket.coverage.cobertura.Line
import com.atlassian.bitbucket.coverage.cobertura.Package
import com.atlassian.bitbucket.coverage.rest.CommitCoverageEntity
import com.atlassian.bitbucket.coverage.rest.FileCoverageEntity
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Path

class CoberturaConverter : AbstractCoverageConverter() {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(JacocoConverter::class.java)
    }

    override fun convert(coverageFile: File, projectPath: Path): CommitCoverageEntity {
        val coverage: Coverage = XmlReader().read(Coverage::class.java, coverageFile)
        val sourceFileResolver = SourceFileResolver(projectPath)

        val files = mutableListOf<FileCoverageEntity?>()

        for (pkg in coverage.packages.`package`) {
            logger.debug("Processing package {}", pkg.name)
            val packageCoverage: List<FileCoverageEntity?> = convertPackage(pkg, sourceFileResolver)
            files.addAll(packageCoverage)
        }

        val result = CommitCoverageEntity()
        result.files = files.filterNotNull()
        return result
    }

    fun convertPackage(pkg: Package, sourceFileResolver: SourceFileResolver): List<FileCoverageEntity?> {
        val result: MutableList<FileCoverageEntity?> = mutableListOf()

        for (clazz in pkg.classes.clazz) {
            val clazzCoverage = convertClass(clazz, sourceFileResolver)
            result.add(clazzCoverage)
        }

        return result
    }

    fun convertClass(clazz: Class, sourceFileResolver: SourceFileResolver): FileCoverageEntity? {
        val fileCoverageMap = emptyCoverageMap()

        for (line in clazz.lines.line) {
            val coverageType = convertLine(line)
            fileCoverageMap[coverageType]!!.add(line.number)
        }

        val path = sourceFileResolver.resolveFileName(clazz.filename)
        return convertFileCoverage(path, fileCoverageMap)
    }

    fun convertLine(line: Line): String {
        return if (line.hits == "0") {
            UNCOVERED_KEY
        } else {
            if (line.branch == "true") {
                convertConditionCoverage(line.conditionCoverage)
            } else {
                COVERED_KEY
            }
        }
    }

    fun convertConditionCoverage(conditionCoverage: String): String {
        return if (conditionCoverage.startsWith("100")) {
            COVERED_KEY
        } else {
            PARTLY_COVERED_KEY
        }
    }

}
