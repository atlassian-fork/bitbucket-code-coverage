package com.atlassian.bitbucket.coverage.rest

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.net.URL
import javax.ws.rs.core.HttpHeaders.CONTENT_TYPE
import javax.ws.rs.core.MediaType.APPLICATION_JSON

open class Client(
        bitbucketUrl: URL,
        private val user: String,
        private val password: String,
        private val token: String,
        private val timeout: Int?) {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(Client::class.java)
    }

    val gson = Gson()

    init {
        FuelManager.instance.basePath = "$bitbucketUrl/rest/code-coverage/1.0"
        FuelManager.instance.baseHeaders = mapOf(CONTENT_TYPE to APPLICATION_JSON)

        validateCredentials(user, password, token)
    }

    fun postCoverage(commitId: String, body: CommitCoverageEntity): Result<String, FuelError> {
        val jsonBody = gson.toJson(body)

        logger.debug("Going to post coverage data")
        logger.debug(jsonBody)

        val request: Request = Fuel.post("commits/$commitId").body(jsonBody)

        if (timeout != null) {
            request.timeout(timeout).timeoutRead(timeout)
        }

        if (user.isEmpty()) {
            request.header("Authorization" to "Bearer $token")
        } else {
            request.authenticate(user, password)
        }

        val (_, _, result) = request.responseString()
        return result
    }

    fun validateCredentials(user: String, password: String, token: String) {
        if (user.isEmpty()) {
            if (token.isEmpty()) {
                throw IllegalArgumentException(
                        "Either user/password or token should be provided in order to using Bitbucket REST API")
            }
        } else {
            if (password.isEmpty()) {
                throw IllegalArgumentException(
                        "Both user and password properties should be set in order to using Bitbucket REST API")
            }
        }
    }

}
