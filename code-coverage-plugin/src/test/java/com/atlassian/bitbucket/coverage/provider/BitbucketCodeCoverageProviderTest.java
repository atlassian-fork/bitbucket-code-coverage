package com.atlassian.bitbucket.coverage.provider;

import com.atlassian.bitbucket.codeinsights.coverage.CodeCoverage;
import com.atlassian.bitbucket.codeinsights.coverage.CodeCoverageCallback;
import com.atlassian.bitbucket.codeinsights.coverage.CodeCoverageLevel;
import com.atlassian.bitbucket.codeinsights.coverage.CodeCoverageRequest;
import com.atlassian.bitbucket.coverage.CoverageManager;
import com.atlassian.bitbucket.coverage.CoverageParser;
import com.atlassian.bitbucket.coverage.rest.FileCoverageEntity;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestRef;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Objects;
import java.util.Optional;

import static com.atlassian.bitbucket.coverage.provider.BitbucketCodeCoverageProvider.PROVIDER_KEY;
import static com.atlassian.bitbucket.coverage.provider.BitbucketCodeCoverageProvider.PROVIDER_URL;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

@RunWith(MockitoJUnitRunner.class)
public class BitbucketCodeCoverageProviderTest {

    private static final String COMMIT_ID = "abcdef";
    private static final String PATH = "files/README.md";

    @Mock
    private CodeCoverageCallback callback;
    @Mock
    private CoverageManager coverageManager;
    private BitbucketCodeCoverageProvider provider;
    @Mock
    private CodeCoverageRequest request;

    @Before
    public void setUp() {
        PullRequest pullRequest = mock(PullRequest.class);
        PullRequestRef fromRef = mock(PullRequestRef.class);

        when(fromRef.getLatestCommit()).thenReturn(COMMIT_ID);
        when(pullRequest.getFromRef()).thenReturn(fromRef);
        when(request.getPullRequest()).thenReturn(pullRequest);
        when(request.getPath()).thenReturn(PATH);

        provider = new BitbucketCodeCoverageProvider(coverageManager, new CoverageParser());
    }

    @Test
    public void streamCoverage() {
        FileCoverageEntity coverage = new FileCoverageEntity();
        coverage.setPath(PATH);
        coverage.setCoverage("C:1,2,3,5,11,12,13;P:4,6,7,8,9,10;U:100,99,97");
        when(coverageManager.getCoverage(COMMIT_ID, PATH)).thenReturn(coverage);

        provider.streamCoverage(request, callback);
        verify(callback).onFileStart(PROVIDER_KEY, PROVIDER_URL, PATH);
        verify(callback).onRange(codeCoverage(1, 3, CodeCoverageLevel.FULL, Optional.empty()));
        verify(callback).onRange(codeCoverage(5, 5, CodeCoverageLevel.FULL, Optional.empty()));
        verify(callback).onRange(codeCoverage(11, 13, CodeCoverageLevel.FULL, Optional.empty()));
        verify(callback).onRange(codeCoverage(4, 4, CodeCoverageLevel.PARTIAL, Optional.empty()));
        verify(callback).onRange(codeCoverage(6, 10, CodeCoverageLevel.PARTIAL, Optional.empty()));
        verify(callback).onRange(codeCoverage(97, 97, CodeCoverageLevel.NONE, Optional.empty()));
        verify(callback).onRange(codeCoverage(99, 100, CodeCoverageLevel.NONE, Optional.empty()));
        verify(callback).onFileEnd();
    }

    @Test
    public void streamCoverageWithUnorderedAndDuplicatedLineNumbers() {
        FileCoverageEntity coverage = new FileCoverageEntity();
        coverage.setPath(PATH);
        coverage.setCoverage("U:55,55,54,54,54,55;C:44,44,40,43,41,42,43,41");
        when(coverageManager.getCoverage(COMMIT_ID, PATH)).thenReturn(coverage);

        provider.streamCoverage(request, callback);
        verify(callback).onFileStart(PROVIDER_KEY, PROVIDER_URL, PATH);
        verify(callback).onRange(codeCoverage(40, 44, CodeCoverageLevel.FULL, Optional.empty()));
        verify(callback).onRange(codeCoverage(54, 55, CodeCoverageLevel.NONE, Optional.empty()));
        verify(callback).onFileEnd();
    }

    @Test
    public void streamCoverageWhenFileCoverageIsEmpty() {
        when(coverageManager.getCoverage(COMMIT_ID, PATH)).thenReturn(null);

        provider.streamCoverage(request, callback);
        verifyZeroInteractions(callback);
    }

    private CodeCoverage codeCoverage(int start, int end, CodeCoverageLevel level, Optional<String> message) {
        return argThat(new TypeSafeMatcher<CodeCoverage>() {

            @Override
            protected boolean matchesSafely(CodeCoverage codeCoverage) {
                return codeCoverage.getStart() == start &&
                        codeCoverage.getEnd() == end &&
                        codeCoverage.getCodeCoverageLevel() == level &&
                        Objects.equals(codeCoverage.getMessage(), message);

            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Code coverage ").appendValue(start + ", " + end + ", " + level + ", " + message);
            }
        });
    }
}
