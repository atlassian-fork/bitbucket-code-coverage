package com.atlassian.bitbucket.coverage;

import com.atlassian.bitbucket.codeinsights.report.GetInsightReportRequest;
import com.atlassian.bitbucket.codeinsights.report.InsightReport;
import com.atlassian.bitbucket.codeinsights.report.InsightReportService;
import com.atlassian.bitbucket.codeinsights.report.SetInsightReportRequest;
import com.atlassian.bitbucket.idx.CommitIndex;
import com.atlassian.bitbucket.idx.IndexedCommit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class InsightReportManager {

    static final String PLUGIN = "com.atlassian.bitbucket.code-coverage-plugin";
    static final String PROVIDER_KEY = "bitbucket-code-coverage-provider";

    private static final Logger log = LoggerFactory.getLogger(InsightReportManager.class);
    private static final String TITLE = "Code Coverage for Bitbucket Server";

    private final CommitIndex commitIndex;
    private final InsightReportService insightReportService;

    @Autowired
    public InsightReportManager(CommitIndex commitIndex,
                                InsightReportService insightReportService) {
        this.commitIndex = commitIndex;
        this.insightReportService = insightReportService;
    }

    /**
     * Finds all repositories containing the {@code commitId} and, for each one, creates an Insight Report relating to
     * the code coverage (if a report does not already exist).
     *
     * @param commitId the commit ID
     */
    protected void createInsightReport(String commitId) {
        // Find all repositories containing this commitId.
        IndexedCommit indexedCommit = commitIndex.getCommit(commitId);
        if (indexedCommit == null) {
            log.warn("Unable to create insight report for commit ID {} as it has not been indexed", commitId);
            return;
        }

        indexedCommit.getRepositories().forEach(repository -> {
            // Look for an existing code insight report for the target repository and commitId.
            Optional<InsightReport> insightReport = insightReportService.get(
                    new GetInsightReportRequest.Builder(repository, commitId, PROVIDER_KEY).build());
            if (!insightReport.isPresent()) {
                // Report doesn't exist, so create a new one.
                insightReportService.set(new SetInsightReportRequest.Builder(repository, commitId, PROVIDER_KEY, TITLE)
                        .coverageProviderKey(PLUGIN + ":" + PROVIDER_KEY)
                        .build());
            }
        });
    }

}
