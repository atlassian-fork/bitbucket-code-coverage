package com.atlassian.bitbucket.coverage;

import com.atlassian.bitbucket.coverage.rest.FileCoverageEntity;

import java.util.List;

public class InvalidCoverageException extends RuntimeException {

    private final List<FileCoverageEntity> invalidEntities;

    public InvalidCoverageException(String message, List<FileCoverageEntity> invalidEntries) {
        super(message);
        this.invalidEntities = invalidEntries;
    }

    public List<FileCoverageEntity> getInvalidEntities() {
        return invalidEntities;
    }

}
