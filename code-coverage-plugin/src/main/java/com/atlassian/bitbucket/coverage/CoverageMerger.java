package com.atlassian.bitbucket.coverage;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.COVERED_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.PARTLY_COVERED_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.UNCOVERED_COLUMN;

@Component
public class CoverageMerger {

    public Map<String, Set<String>> mergeCoverage(
            Map<String, Set<String>> oldCoverage, Map<String, Set<String>> newCoverage) {
        Set<String> coveredLines = mergeCoverage(
                oldCoverage.get(COVERED_COLUMN), newCoverage.get(COVERED_COLUMN));
        Set<String> partlyCoveredLines = mergeCoverage(
                oldCoverage.get(PARTLY_COVERED_COLUMN), newCoverage.get(PARTLY_COVERED_COLUMN));
        Set<String> uncoveredLines = mergeCoverage(
                oldCoverage.get(UNCOVERED_COLUMN), newCoverage.get(UNCOVERED_COLUMN));

        uncoveredLines.removeAll(coveredLines);
        uncoveredLines.removeAll(partlyCoveredLines);
        partlyCoveredLines.removeAll(coveredLines);

        Map<String, Set<String>> result = new HashMap<>();
        result.put(COVERED_COLUMN, coveredLines);
        result.put(PARTLY_COVERED_COLUMN, partlyCoveredLines);
        result.put(UNCOVERED_COLUMN, uncoveredLines);

        return result;
    }

    protected Set<String> mergeCoverage(Set<String> existingCoverage, Set<String> newCoverage) {
        Set<String> result = new HashSet<>();
        result.addAll(existingCoverage);
        result.addAll(newCoverage);
        return result;
    }

}
