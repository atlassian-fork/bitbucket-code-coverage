Code Coverage Plugin for Bitbucket server Pull-Requests
=======================================================

This plugin adds 2 simple feature to your Bitbucket server installation:

1. REST endpoints to provide coverage information for your code
2. UI module to display this information in pull-requests

Usage
=====

Build and install plugin on your Bitbucket server instance
----------------------------------------------------------

This plugin is freely available on [Atlassian Marketplace](https://marketplace.atlassian.com/apps/1218271/code-coverage-for-bitbucket-server?hosting=server&tab=overview).

Alternatively, you could build it from sources:
 
 * Run `mvn clean package`. This will produce `jar` file inside `target` directory. 
 * Use [UPM](https://marketplace.atlassian.com/plugins/com.atlassian.upm.atlassian-universal-plugin-manager-plugin/server/overview) to install it on your Bitbucket server.
 
To use with Bitbucket 5.x please checkout code revision tagged with [bitbucket-code-coverage-1.0.13](https://bitbucket.org/atlassian/bitbucket-code-coverage/commits/tag/bitbucket-code-coverage-1.0.13)

Post coverage data to REST endpoint
-----------------------------------

There is a [maven plugin](../code-coverage-maven-plugin) to process and publish coverage data.

However, if maven is not a part of your toolchain, you can use any tool to make REST call to Bitbucket:

```bash
curl -X POST -H "Content-Type: application/json" --data @test.json -u user:password "http://localhost:7990/bitbucket/rest/code-coverage/1.0/commits/cdddd92170cd44071217c41b5020f9a26f04c91f"
```

Example data json:

```json
{
  "files": [
    {
	  	"path": "test.txt",
 		"coverage": "C:1,2,3,4,5;P:6,7,8;U:9"
    }
  ]
}
```

`path` is relative to project root
`coverage` consists of 3 semi-column separated parts:
 * `C` - list of fully covered lines
 * `P` - list of partly covered lines (line considered partly covered when it has multiple branches, some of them covered and some not)
 * `U` - list of uncovered lines

You can post multiple times for same commit hash (e.g. from different test types) - coverage data will be merged.

Cleanup
-------

Once pull-request is Merged, Deleted or Declined, all corresponding coverage data will be deleted.
