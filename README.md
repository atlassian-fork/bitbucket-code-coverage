Bitbucket Server Code Coverage
==============================

There are a lot of tools to create and visualize code coverage information. But usually it's available only as external
report after code hit master branch. This project brings coverage information to the point where it's mostly needed -
in your pull requests:

![Example of pull-request with coverage information](pr-coverage-example.png)

There are two major modules in project:

1. [Bitbucket server plugin](code-coverage-plugin) to store and display code coverage information in pull-requests.
For detailed instruction on how to add plugin to your Bitbucket server, please read corresponding [README](code-coverage-plugin/README.md).
2. [Maven plugin](code-coverage-maven-plugin) to convert coverage data from existing tools (e.g. Istanbul) and publish
then to Bitbucket. Read [module's README](code-coverage-maven-plugin/README.md) for usage details.

Contributors
============

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

Test your changes locally before creating pull request. To run tests with maven:

```bash
mvn test
```

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
=======

Copyright (c) 2017 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
